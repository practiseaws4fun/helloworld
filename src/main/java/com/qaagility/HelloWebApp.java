package com.qaagility;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingListener;
import java.util.Date;

public class HelloWebApp extends HttpServlet {

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {

            PrintWriter out = resp.getWriter();
            
            resp.setContentType("text/html");

            out.println("<html>");
            out.println("<body bgcolor=\"Yellow\">");

            out.println("<h1>Hello World in DevOps style.</h1>");
            resp.getWriter().write("Test DevOps project by  Hitesh Kumar ..\n\n\n");

            out.println("</body>");
            out.println("</html>");   
        }


        public int add(int a, int b) {
            return a + b;
        }

	public int sub(int a, int b) {
            int res;
            return a - b;
        }

}
